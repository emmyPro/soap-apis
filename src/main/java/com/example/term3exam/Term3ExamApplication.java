package com.example.term3exam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Term3ExamApplication {

    public static void main(String[] args) {
        SpringApplication.run(Term3ExamApplication.class, args);
    }

}
