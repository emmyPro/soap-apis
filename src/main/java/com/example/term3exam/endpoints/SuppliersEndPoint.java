package com.example.term3exam.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import com.example.term3exam.repositories.SupplierRepository;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;
import java.util.Optional;

@Endpoint
public class SuppliersEndPoint {
    private final SupplierRepository supplierRepository;

    @Autowired
    public void SupplierEndPoint(SupplierRepository repository) {
        this.supplierRepository = repository;
    }

    public SuppliersEndPoint(SupplierRepository supplierRepository) {
        this.supplierRepository = supplierRepository;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/kwizera/Term3-exam", localPart = "NewSupplierRequest")
    @ResponsePayload
    public NewSupplierResponse create(@RequestPayload NewSupplierRequest dto) {
        jaxb.classes.Supplier __supplier = dto.getSupplier();

        Supplier _supplier = mapSupplier(__supplier);

        Supplier supplier = supplierRepository.save(__supplier);

        NewSupplierResponse response = new NewSupplierResponse();

        __supplier.setId(supplier.getId());

        response.setSupplier(__supplier);

        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/kwizera/Term3-exam", localPart = "GetAllSuppliersRequest")
    @ResponsePayload
    public GetAllSuppliersResponse findAll(@RequestPayload GetAllSuppliersRequest request) {

        List<Supplier> supplier = supplierRepository.findAll();

        GetAllSuppliersResponse response = new GetAllSuppliersResponse();

        for (Supplier supplier : supplier) {
            jaxb.classes.Supplier _supplier = mapSupplier(supplier);

            response.getSupplier().add(_supplier);
        }

        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/kwizera/Term3-exam", localPart = "GetSupplierByIdRequest")
    @ResponsePayload
    public GetSupplierByIdResponse findById(@RequestPayload GetSupplierByIdRequest request) {
        Optional<Supplier> _supplier = supplierRepository.findById(request.getId());

        if (!_supplier.isPresent())
            return new GetSupplierByIdResponse();

        Supplier supplier = _supplier.get();

        GetSupplierByIdResponse response = new GetSupplierByIdResponse();

        jaxb.classes.Supplier __supplier = mapSupplier(supplier);

        response.setSupplier(__supplier);

        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/kwizera/Term3-exam", localPart = "DeleteSupplierRequest")
    @ResponsePayload
    public DeleteSupplierResponse delete(@RequestPayload DeleteSupplierRequest request) {
        supplierRepository.deleteById(request.getId());
        DeleteSupplierResponse response = new DeleteSupplierResponse();
        response.setMessage("Successfully deleted a message");
        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/kwizera/Term3-exam", localPart = "UpdateSupplierRequest")
    @ResponsePayload
    public UpdateSupplierResponse update(@RequestPayload UpdateSupplierRequest request) {
        jaxb.classes.Supplier __supplier = request.getSupplier();

        Supplier _supplier = mapSupplier(__supplier);
        _supplier.setId(__supplier.getId());

        Supplier supplier = supplierRepository.save(_supplier);

        UpdateSupplierResponse supplierDTO = new UpdateSupplierResponse();

        __supplier.setId(supplier.getId());

        supplierDTO.setSupplier(__supplier);

        return supplierDTO;
    }

    private jaxb.classes.Supplier mapSupplier(Supplier supplier) {
        jaxb.classes.Supplier _supplier = new jaxb.classes.Supplier();
        _supplier.setId(supplier.getId());
        _supplier.setNames(supplier.getNames());
        _supplier.getEmail(supplier.getEmail());
        _supplier.setMobile(supplier.getMobile());

        return _supplier;
    }

    private Supplier mapSupplier(jaxb.classes.Supplier __supplier) {
        return new Supplier(__supplier.getId(), __supplier.getNames(), __supplier.getEmail(), __supplier.getMobile());
    }
}
