package com.example.term3exam.models;

import javax.persistence.*;

@Entity
@Table(name = "items")
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long itemId;

    public String name;
    public String itemCode;
    public Double price;
    public long supplier_id;

    public Item() {
    }

    public Item(long itemId, String name, String itemCode, Double price, long supplier_id) {
        this.itemId = itemId;
        this.name = name;
        this.itemCode = itemCode;
        this.price = price;
        this.supplier_id = supplier_id;
    }

    public Item(String name, String itemCode, Double price, long supplier_id) {
        this.name = name;
        this.itemCode = itemCode;
        this.price = price;
        this.supplier_id = supplier_id;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long id) {
        this.itemId = itemId;
    }

    public String getNames() {
        return name;
    }

    public void setNames(String names) {
        this.names = name;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public long getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(long supplier_id) {
        this.supplier_id = supplier_id;
    }
}
