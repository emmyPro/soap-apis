package com.example.term3exam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.term3exam.models.Supplier;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier, Long> {
}
